﻿namespace BlazorMVVM.Sample.Client.Shared
{
    public class SurveyPromptModel : ParameterAwareBase
    {
        public string TitleParameter => GetParameter<string>("Title");
    }
}