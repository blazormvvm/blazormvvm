﻿namespace BlazorMVVM.Sample.Client.Shared
{
    public class NavMenuModel : PropertyChangedBase
    {
        private bool _collapseNavMenu = true;

        public string NavMenuCssClass => _collapseNavMenu ? "collapse" : null;

        public void ToggleNavMenu()
        {
            _collapseNavMenu = !_collapseNavMenu;
            RaisePropertyChanged(nameof(NavMenuCssClass));
        }
    }
}