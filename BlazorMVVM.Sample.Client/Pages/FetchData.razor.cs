﻿using BlazorMVVM.Awareness;
using BlazorMVVM.Sample.Shared;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorMVVM.Sample.Client.Pages
{
    public class FetchDataModel : PropertyChangedBase, IInitAwareAsync
    {
        private readonly HttpClient _httpClient;

        private WeatherForecast[] _forecasts;

        public WeatherForecast[] Forecasts
        {
            get => _forecasts;
            private set => SetProperty(ref _forecasts, value);
        }

        public FetchDataModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task OnInitializedAsync()
        {
            await UpdateForecasts();
        }

        private async Task UpdateForecasts()
        {
            Forecasts = await _httpClient.GetJsonAsync<WeatherForecast[]>("api/SampleData/WeatherForecasts");
        }
    }
}