﻿namespace BlazorMVVM.Sample.Client.Pages
{
    public class CounterModel : PropertyChangedBase
    {
        private int _currentCount;

        public int CurrentCount
        {
            get => _currentCount;
            private set => SetProperty(ref _currentCount, value);
        }

        public void IncrementCount()
        {
            CurrentCount++;
        }
    }
}