[![pipeline status](https://gitlab.com/blazormvvm/blazormvvm/badges/master/pipeline.svg)](https://gitlab.com/blazormvvm/blazormvvm/commits/master)
[![nuget version](https://img.shields.io/nuget/v/BlazorMVVM)](https://www.nuget.org/profiles/BlazorMVVM)

# BlazorMVVM

BlazorMVVM is an MVVM framework for Blazor.

## Installation

Use the Visual Studio package manager to install `BlazorMVVM`.

Or use the following command:

```bash
Install-Package BlazorMVVM
```

If your Views and ViewModels are in separate projects, you may install the `BlazorMVVM.Core` package into your ViewModels project, and the `BlazorMVVM.Components` package into your Views project, instead of the `BlazorMVVM` package.

## Usage

### Overview

Views should inherit from `ComponentBase<TViewModel>`. ViewModels should be added to the dependency injection container as transient. ViewModels will be created and injected into views that inherit from `ComponentBase<TViewModel>` at runtime.

### Layouts

Layouts that require a ViewModel should inherit from `LayoutComponentBase<TViewModel>`.

### ViewModels

ViewModels don't need to inherit from a base class. But they may inherit from `PropertyChangedBase` for a standard implementation of `INotifyPropertyChanged`.

The View base classes will call `StateHasChanged()` when their ViewModel's `PropertyChanged` event is raised.

#### Parameters

ViewModels may access parameters by inheriting from `ParameterAwareBase`, which provides a `GetParameter<T>()` method as well as `virtual OnParameterChanged()` and `virtual OnParameterChangedAsync()` methods. `ParameterAwareBase` is aware of anything passed in the `parameters` argument of `SetParametersAsync(ParameterCollection parameters)`, including Cascading Parameters.

##### Example:

```html
@inherits ComponentBase<ParameterExampleModel>
@code{ [Parameter] public string Title { get; set; } }

My Title parameter is '@Model.TitleParameter' and my MyTitle property is '@Model.MyTitle'
```

```cs
public class ParameterExampleModel : ParameterAwareBase
{
    public string TitleParameter => GetParameter<string>("Title");

    private string _myTitle;
    public string MyTitle
    {
        get => _myTitle;
        private set => SetProperty(ref _myTitle, value);
    }

    protected override void OnParameterChanged(string name, object newValue, object oldValue)
    {
        if (name == "Title") MyTitle = "MyTitle: " + newValue;
    }
}
```

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT No Attribution (MIT-0)](https://gitlab.com/blazormvvm/blazormvvm/blob/master/LICENSE)
