﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace BlazorMVVM.Components
{
    public static class ConfigurationExtensions
    {
        public static void AutoRegisterViewModels(this IServiceCollection services, Assembly assembly)
        {
            var viewTypes = assembly.GetTypes()
                .Where(t => t.IsComponentBase() && !t.IsAbstract)
                .ToList();

            foreach (var viewType in viewTypes)
            {
                var viewModelType = viewType.GetViewModelType();

                services.AddTransient(viewModelType);
            }
        }
    }
}