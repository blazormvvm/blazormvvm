﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using BlazorMVVM.Awareness;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using IComponent = Microsoft.AspNetCore.Components.IComponent;

namespace BlazorMVVM.Components
{
    public abstract class ComponentBase<TViewModel> : IComponent, IHandleEvent, IHandleAfterRender, IDisposable
        where TViewModel : class
    {
        private readonly RenderFragment _renderFragment;
        private bool _hasNeverRendered = true;
        private bool _hasPendingQueuedRender;
        private bool _initialized;
        private TViewModel _model;
        private RenderHandle _renderHandle;

        [Inject]
        public TViewModel Model
        {
            get => _model;
            set
            {
                if (EqualityComparer<TViewModel>.Default.Equals(_model, value)) return;

                if (_model is INotifyPropertyChanged oldViewModel)
                    oldViewModel.PropertyChanged -= Model_OnPropertyChanged;

                if (value is INotifyPropertyChanged newViewModel)
                    newViewModel.PropertyChanged += Model_OnPropertyChanged;

                _model = value;
            }
        }

        public virtual bool ShouldRender => true;

        protected ComponentBase()
        {
            _renderFragment = builder =>
            {
                _hasPendingQueuedRender = false;
                _hasNeverRendered = false;

                BuildRenderTree(builder);
            };
        }

        void IComponent.Attach(RenderHandle renderHandle)
        {
            if (_renderHandle.IsInitialized) throw new InvalidOperationException("Render handle already set.");

            _renderHandle = renderHandle;
        }

        Task IComponent.SetParametersAsync(ParameterView parameters)
        {
            parameters.SetParameterProperties(this);
            var changes = UpdateModelParameters(parameters);

            if (!_initialized)
            {
                _initialized = true;

                return RaiseOnInitializedAndParametersSet(changes);
            }

            return RaiseOnParametersSet(changes);
        }

        public virtual void Dispose()
        {
            if (Model is INotifyPropertyChanged model) model.PropertyChanged -= Model_OnPropertyChanged;
        }

        Task IHandleAfterRender.OnAfterRenderAsync()
        {
            OnAfterRender();
        
            return OnAfterRenderAsync();
        }
        
        Task IHandleEvent.HandleEventAsync(EventCallbackWorkItem callback, object arg)
        {
            var task = callback.InvokeAsync(arg);
            var shouldAwaitTask = task.Status != TaskStatus.RanToCompletion && task.Status != TaskStatus.Canceled;
        
            StateHasChanged();
        
            return shouldAwaitTask ? CallStateHasChangedOnAsyncCompletion(task) : Task.CompletedTask;
        }

        protected virtual void OnInitialized()
        {
        }

        protected virtual Task OnInitializedAsync()
        {
            return Task.CompletedTask;
        }

        protected virtual void OnParametersSet()
        {
        }

        protected virtual Task OnParametersSetAsync()
        {
            return Task.CompletedTask;
        }

        protected virtual void OnAfterRender()
        {
        }

        protected virtual Task OnAfterRenderAsync()
        {
            return Task.CompletedTask;
        }

        protected virtual void BuildRenderTree(RenderTreeBuilder builder)
        {
        }

        protected void StateHasChanged()
        {
            if (_hasPendingQueuedRender) return;

            if (_hasNeverRendered || ShouldRender)
            {
                _hasPendingQueuedRender = true;

                try
                {
                    _renderHandle.Render(_renderFragment);
                }
                catch
                {
                    _hasPendingQueuedRender = false;

                    throw;
                }
            }
        }

        protected Task InvokeAsync(Action workItem)
        {
            return _renderHandle.Dispatcher.InvokeAsync(workItem);
        }

        protected Task InvokeAsync(Func<Task> workItem)
        {
            return _renderHandle.Dispatcher.InvokeAsync(workItem);
        }

        private IList<ParameterChange> UpdateModelParameters(ParameterView parameters)
        {
            if (Model is IParameterAware m) return m.UpdateParameters(parameters.ToDictionary());

            return null;
        }

        private async Task RaiseOnInitializedAndParametersSet(IList<ParameterChange> changes)
        {
            OnInitialized();
            await OnInitializedAsync();

            var task = RaiseModelInitialized();

            if (task.Status != TaskStatus.RanToCompletion && task.Status != TaskStatus.Canceled)
            {
                StateHasChanged();

                try
                {
                    await task;
                }
                catch // Avoiding exception filters for AOT runtime support
                {
                    if (!task.IsCanceled) throw;
                }
            }

            await RaiseOnParametersSet(changes);
        }

        private async Task RaiseOnParametersSet(IList<ParameterChange> changes)
        {
            OnParametersSet();
            await OnParametersSetAsync();

            await RaiseModelParametersChanged(changes);

            StateHasChanged();
        }

        private Task RaiseModelInitialized()
        {
            if (Model is IInitAware m) m.OnInitialized();
            if (Model is IInitAwareAsync ma) return ma.OnInitializedAsync();

            return Task.CompletedTask;
        }

        private Task RaiseModelParametersChanged(IList<ParameterChange> changes)
        {
            if (changes != null && Model is IParameterAware m) return m.ParametersChanged(changes);

            return Task.CompletedTask;
        }

        private async Task CallStateHasChangedOnAsyncCompletion(Task task)
        {
            try
            {
                await task;
            }
            catch // Avoiding exception filters for AOT runtime support
            {
                if (task.IsCanceled) return;

                throw;
            }

            StateHasChanged();
        }

        private void Model_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            StateHasChanged();
        }
    }
}