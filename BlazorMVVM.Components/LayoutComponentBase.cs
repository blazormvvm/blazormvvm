﻿using Microsoft.AspNetCore.Components;

namespace BlazorMVVM.Components
{
    public class LayoutComponentBase<TViewModel> : ComponentBase<TViewModel>
        where TViewModel : class
    {
        internal const string BodyPropertyName = nameof(Body);

        [Parameter]
        public RenderFragment Body { get; set; }
    }
}