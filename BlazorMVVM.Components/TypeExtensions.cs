﻿using System;

namespace BlazorMVVM.Components
{
    public static class TypeExtensions
    {
        public static bool IsComponentBase(this Type type)
        {
            var componentBaseType = typeof(ComponentBase<>);

            if (type == null || type == typeof(object)) return false;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == componentBaseType) return true;

            return type.BaseType.IsComponentBase();
        }

        public static Type GetViewModelType(this Type type)
        {
            var componentBaseType = typeof(ComponentBase<>);

            if (type == null || type == typeof(object)) return null;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == componentBaseType)
            {
                return type.GetGenericArguments()[0];
            }

            return type.BaseType.GetViewModelType();
        }
    }
}