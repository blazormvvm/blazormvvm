﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorMVVM
{
    public interface IParameterAware
    {
        IList<ParameterChange> UpdateParameters(IReadOnlyDictionary<string, object> newParameters);

        Task ParametersChanged(IList<ParameterChange> changes);
    }
}