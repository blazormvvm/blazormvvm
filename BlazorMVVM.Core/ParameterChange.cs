﻿namespace BlazorMVVM
{
    public sealed class ParameterChange
    {
        public string Name { get; }
        public object NewValue { get; }
        public object OldValue { get; }

        public ParameterChange(string name, object newValue, object oldValue)
        {
            Name = name;
            NewValue = newValue;
            OldValue = oldValue;
        }
    }
}