﻿namespace BlazorMVVM.Awareness
{
    public interface IInitAware
    {
        void OnInitialized();
    }
}