﻿using System.Threading.Tasks;

namespace BlazorMVVM.Awareness
{
    public interface IInitAwareAsync
    {
        Task OnInitializedAsync();
    }
}