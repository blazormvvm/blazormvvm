﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorMVVM
{
    public abstract class ParameterAwareBase : PropertyChangedBase, IParameterAware
    {
        private IReadOnlyDictionary<string, object> _parameters;

        protected T GetParameter<T>(string name)
        {
            if (!_parameters.ContainsKey(name)) return default;

            return (T)_parameters[name];
        }

        protected virtual void OnParametersChanging() { }

        protected virtual Task OnParametersChangingAsync() => Task.CompletedTask;

        protected virtual void OnParameterChanged(ParameterChange change) { }

        protected virtual Task OnParameterChangedAsync(ParameterChange change) =>
            Task.CompletedTask;

        protected virtual void OnParametersChanged() { }

        protected virtual Task OnParametersChangedAsync() => Task.CompletedTask;

        IList<ParameterChange> IParameterAware.UpdateParameters(IReadOnlyDictionary<string, object> newParameters)
        {
            var changes = new List<ParameterChange>();

            foreach (var kvp in newParameters)
            {
                var name = kvp.Key;
                var newValue = kvp.Value;
                object oldValue = null;

                if (_parameters != null && _parameters.ContainsKey(kvp.Key))
                {
                    oldValue = _parameters[kvp.Key];

                    if (newValue == oldValue) continue;
                }

                changes.Add(new ParameterChange(name, newValue, oldValue));
            }

            _parameters = newParameters;

            return changes;
        }

        async Task IParameterAware.ParametersChanged(IList<ParameterChange> changes)
        {
            OnParametersChanging();
            await OnParametersChangingAsync();

            foreach (var change in changes)
            {
                OnParameterChanged(change);

                await OnParameterChangedAsync(change);
            }

            OnParametersChanged();
            await OnParametersChangedAsync();
        }
    }
}