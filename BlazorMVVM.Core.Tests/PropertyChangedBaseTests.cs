﻿using Shouldly;
using System.Runtime.CompilerServices;
using Xunit;

namespace BlazorMVVM.Core.Tests
{
    public class PropertyChangedBaseTests
    {
        [Fact]
        public void RaisePropertyChanged_PropertyChangedIsSet_ShouldInvoke()
        {
            const string expectedPropertyName = "TestProperty";
            var count = 0;
            var propertyName = "";
            var sut = new MockViewModel();
            sut.PropertyChanged += (sender, args) =>
            {
                propertyName = args.PropertyName;
                count++;
            };

            sut.RaisePropertyChanged(expectedPropertyName);

            propertyName.ShouldBe(expectedPropertyName);
            count.ShouldBe(1);
        }

        [Fact]
        public void SetProperty_ValueNotChanged_ShouldNotRaisePropertyChanged()
        {
            const string expectedPropertyName = "TestProperty";
            var count = 0;
            var sut = new MockViewModel();
            sut.PropertyChanged += (sender, args) =>
            {
                count++;
            };

            var property = "test";
            sut.SetProperty(ref property, property, expectedPropertyName);

            count.ShouldBe(0);
        }

        [Fact]
        public void SetProperty_ValueNotChanged_ShouldReturnFalse()
        {
            var sut = new MockViewModel();

            var property = "test";
            var result = sut.SetProperty(ref property, property);

            result.ShouldBeFalse();
        }

        [Fact]
        public void SetProperty_ValueChanged_ShouldRaisePropertyChanged()
        {
            const string expectedPropertyName = "TestProperty";
            var count = 0;
            var propertyName = "";
            var sut = new MockViewModel();
            sut.PropertyChanged += (sender, args) =>
            {
                propertyName = args.PropertyName;
                count++;
            };

            var property = "test";
            sut.SetProperty(ref property, "", expectedPropertyName);

            propertyName.ShouldBe(expectedPropertyName);
            count.ShouldBe(1);
        }

        [Fact]
        public void SetProperty_ValueChanged_ShouldReturnTrue()
        {
            var sut = new MockViewModel();

            var property = "test";
            var result = sut.SetProperty(ref property, "");

            result.ShouldBeTrue();
        }

        private class MockViewModel : PropertyChangedBase
        {
            public new bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
            {
                return base.SetProperty(ref storage, value, propertyName);
            }

            public new void RaisePropertyChanged([CallerMemberName] string propertyName = null)
            {
                base.RaisePropertyChanged(propertyName);
            }
        }
    }
}