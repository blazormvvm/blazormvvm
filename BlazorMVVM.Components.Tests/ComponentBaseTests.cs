﻿using NSubstitute;
using System.ComponentModel;
using Xunit;

namespace BlazorMVVM.Components.Tests
{
    public class ComponentBaseTests
    {
        [Fact]
        public void ModelSet_NewValueIsINotifyPropertyChanged_ShouldSubscribeToPropertyChanged()
        {
            var model = Substitute.For<INotifyPropertyChanged>();
            var sut = Substitute.For<ComponentBase<INotifyPropertyChanged>>();

            sut.Model = model;

            model.Received(1).PropertyChanged += Arg.Any<PropertyChangedEventHandler>();
        }

        [Fact]
        public void ModelSet_OldValueIsINotifyPropertyChanged_ShouldUnsubscribeFromPropertyChanged()
        {
            var model = Substitute.For<INotifyPropertyChanged>();
            var sut = Substitute.For<ComponentBase<INotifyPropertyChanged>>();
            sut.Model = model;

            sut.Model = null;

            model.Received(1).PropertyChanged -= Arg.Any<PropertyChangedEventHandler>();
        }

        [Fact]
        public void ModelSet_NewValueIsSame_ShouldNotSubscribeToPropertyChanged()
        {
            var model = Substitute.For<INotifyPropertyChanged>();
            var sut = Substitute.For<ComponentBase<INotifyPropertyChanged>>();
            sut.Model = model;
            model.ClearReceivedCalls();

            sut.Model = model;

            model.DidNotReceive().PropertyChanged += Arg.Any<PropertyChangedEventHandler>();
        }

        [Fact]
        public void ModelSet_NewValueIsSame_ShouldNotUnsubscribeFromPropertyChanged()
        {
            var model = Substitute.For<INotifyPropertyChanged>();
            var sut = Substitute.For<ComponentBase<INotifyPropertyChanged>>();
            sut.Model = model;
            model.ClearReceivedCalls();

            sut.Model = model;

            model.DidNotReceive().PropertyChanged -= Arg.Any<PropertyChangedEventHandler>();
        }
    }
}